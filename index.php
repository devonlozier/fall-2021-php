<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Devon Lozier's Assignment Homepage</title>
        <link rel="stylesheet" type="text/css" href="css/base.css">
    </head>
    <body>
        <header><?php include('includes/header.php') ?></header>

        <nav><?php include('includes/nav.php') ?></nav>

        <main>
            <img src="img/cat-blob-coffee.png" alt="My picture" width="128" height="128"/>
            <h2>Content about me:</h2>
            <p> I play games and am a student in software development. <br>
                Stuff and whatever, maybe I should have used some lorem ipsum. <br>
                The current class this is for is PHP.
            </p>
        </main>

        <footer><?php include('includes/footer.php') ?></footer>
    </body>
</html>
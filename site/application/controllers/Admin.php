<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
    {
        $data = [
            'dashboard' => true,
        ];

        $this->load->view('admin/dashboard', $data);
    }

    /*
     *
     * T2 = registration form code
     * T5 = Add Marathon
     * T7 = Manage Runners, Manage Marathons, and Dashboard
     *
     */

    public function manage_marathons(){

        $this->load->model('Race');

        $data = [
            'manage_marathons' => true,
            'races' => $this->Race->get_races()
        ];

//        print_r($this->Race->get_races());exit;

        $this->load->view('admin/manage_marathons', $data);
    }

    public function add_marathon(){
        $data = ['add_marathon' => true];
        $this->load->view('admin/add_marathon', $data);
    }

    public function add_race() {
        $this->load->model('Race');

        $this->Race->add_race(
            $this->input->post('txt_name'),
            $this->input->post('txt_location'),
            $this->input->post('txt_description'),
            $this->input->post('txt_date')
        );

        redirect('admin/manage_marathons', 'refresh');
    }

    public function delete_race($id) {
        $this->load->model('Race');

        $this->Race->delete_race($id);

        redirect('admin/manage_marathons', 'refresh');
    }

    public function update_race($id) {
        $this->load->model('Race');
        $race = $this->Race->get_race($id);
        $this->load->view('admin/update_marathon', ['race' => $race]);
    }

    public function edit_race() {
        $this->load->model('Race');
//        echo($this->input->post('txt_id'));exit;

        $this->Race->update_race(
            $this->input->post('txt_name'),
            $this->input->post('txt_location'),
            $this->input->post('txt_description'),
            $this->input->post('txt_date'),
            $this->input->post('txt_id')
        );

        redirect('admin/manage_marathons', 'refresh');
    }

    public function manage_runners() {
        $data = ['manage_runners' => true];
        $this->load->view('admin/manage_runners', $data);
    }

    public function registration_form() {
        $data = ['registration_form' => true];
        $this->load->view('admin/registration_form', $data);
    }

    public function register_marathon() {

    }

    /*
    public function view_1(){
        $this->load->view('admin/1');
    }

    public function view_2(){
        $this->load->view('admin/2');
    }

    public function view_3(){
        $this->load->view('admin/3');
    }

    public function view_4(){
        $this->load->view('admin/4');
    }

    public function view_5(){
        $this->load->view('admin/5');
    }

    public function view_6(){
        $this->load->view('admin/6');
    }

    public function view_7(){
        $this->load->view('admin/7');
    }
    */

}

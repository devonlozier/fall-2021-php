<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= asset_url(); ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= asset_url(); ?>css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= asset_url(); ?>font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php
            $this->load->view('includes/header');
            $this->load->view('includes/menu');
            ?>
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Marathon
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Forms
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">

<!--                        <form role="form">-->
<!--                            <h1>Add Marathon</h1>-->
<!--                            <fieldset>-->
<!--                                <legend>Add Marathon</legend>-->
<!--                                <input type="text" name="race_name" placeholder="race name"> <br>-->
<!--                                <input type="text" name="race_location" placeholder ="City, State, Address"> <br>-->
<!--                                <input type="text" name="race_description" placeholder="Tell us about your race!"> <br>-->
<!--                                <input type="text" name="race_date_time" placeholder="date/time: yyyy-mm-dd hh:mm:ss"> <br>-->
<!--                                <input type="submit" name="submit-race" value="Submit">-->
<!--                            </fieldset>-->
<!--                        </form>-->

                        <form role="form" method="POST" action="/site/admin/add_race">
                            <div class="form-group">

                                <label for="txt_name">Race Name</label>
                                <input type="text" name="txt_name" class="form-control"/>
                                <br/>

                                <label for="txt_location">Race Location</label>
                                <input type="text" name="txt_location" class="form-control"/>
                                <br/>

                                <label for="txt_description">Race Description</label>
                                <textarea name="txt_description" class="form-control"></textarea>
                                <br/>

                                <label for="txt_date">Race Date/Time</label>
                                <input type="text" name="txt_date" class="form-control"/>
                                <br/>

                                <button type="submit" class="btn btn-default">Add Race</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>

                        </form>

<!--                        <php-->
<!--                        echo form_open('admin/register_marathon');-->
<!--                        echo form_input('race_name', '', 'placeholder="race name"').'<br/>';-->
<!--                        echo form_input('race_location', '', 'placeholder="City, State, Address"').'<br/>';-->
<!--                        echo form_input('race_description', '', 'placeholder="Tell us about your race!"').'<br/>';-->
<!--                        echo form_input('race_date_time', '', 'placeholder="date/time: yyyy-mm-dd hh:mm:ss"').'<br/>';-->
<!--                        echo form_submit('submit', 'register', ['class' => 'btn btn-primary']);-->
<!--                        echo form_close();-->
<!--                        >-->

                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?= asset_url(); ?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= asset_url(); ?>js/bootstrap.min.js"></script>

</body>

</html>

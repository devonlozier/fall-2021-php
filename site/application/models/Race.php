<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Race extends CI_Model {

    /**
     * Get all race data from the race_events table
     *
     * @return mixed
     */
    public function get_races() {
        $this->load->database();

        try{
            $query = $this->db->get('race_events');
            return $query->result_array();

        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Get data for a single race_event record.
     *
     * @param int $id (The primary key of the desired race event)
     */
    public function get_race($id) {
        $this->load->database();

        try{
            $data = ['race_id' => $id];
            $query = $this->db->get_where('race_events', $data);
            return $query->result_array();

        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Add a new race to the phpclass.race_events table.
     *
     * @param string $name (Name of race)
     * @param string $location (Where the race is held)
     * @param string $description (Information about the race)
     * @param string $date (Format: YYYY-MM-DD HH:MM:SS)
     */
    public function add_race($name, $location, $description, $date) {
        $this->load->database();

        try{
            $data = [
                'race_name' => $name,
                'race_location' => $location,
                'race_description' => $description,
                'race_date_time' => $date
            ];
            $query = $this->db->insert('race_events', $data);
//            $this->db->insert('race_events', $data);

        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Update a race_event record
     *
     * @param string $name
     * @param string $location
     * @param string $description
     * @param  string $date (Format: YYYY-MM-DD HH:MM:SS)
     * @param int $id (The desired race_id from the race_events table)
     */
    public function update_race($name, $location, $description, $date, $id) {
//        echo($name); echo($location); echo($description); echo($date); echo($id); exit;

        $this->load->database();

        try{
            $data = [
                'race_name' => $name,
                'race_location' => $location,
                'race_description' => $description,
                'race_date_time' => $date
            ];
            $this->db->where('race_id', $id);
            $query = $this->db->update('race_events', $data);

        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /**
     * Delete race event from the race_events table
     *
     * @param int $id (Primary key of the desired race event to remove)
     */
    public function delete_race($id) {
        $this->load->database();

        try{
            $data = [ 'race_id' => $id];
            $query = $this->db->delete('race_events', $data);
        } catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

    public function user_login($email, $pwd){
//        echo $email . " => " . $pwd;exit();
        $this->load->database();
        $this->load->library('session');

        try {
            //-- look up the user data by email address in the member table
            $db = new PDO ($this->db->dsn,$this->db->username, $this->db->password, $this->db->options);
            $sql = $db->prepare('
            SELECT
              password,
              member_key
            FROM
              phpclass.member_login
            WHERE
              email = :Email
              AND role_id = 2
        ');
            $sql->bindValue(':Email', $email);
            $sql->execute();
            $row = $sql->fetch();

            if ($row != null) {
                //--check passwords if email exists
                $hashed_password = md5($pwd . $row['member_key']);

                if ($hashed_password == $row['password']) {
//                    $_SESSION['UID'] = $row['member_key'];
//                    $_SESSION['ROLE'] = $row['role_id'];
//                    header("Location:admin.php");
//                    exit;

                    $this->session->set_userdata(['UID' => $row['member_key']]);
                    return true;
                } else {
                    return false;
                }
            }else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();exit;
            return false;
        }
    }

    public function user_register($fullname,$email,$pwd){
//        return true;
//        echo $fullname . " => " . $email . " => " . $pwd . " => "; exit();
//        $this->load->database();
//        $this->load->library('session');
//
        //--database stuff
        try{
            $this->load->database();

            $member_key = sprintf(
                '%04X%04X%04X%04X%04X%04X%04X%04X',
                mt_rand(0, 65535),
                mt_rand(0, 65535),
                mt_rand(0, 65535),
                mt_rand(16384, 20479),
                mt_rand(32768, 49151),
                mt_rand(0, 65535),
                mt_rand(0, 65535),
                mt_rand(0, 65535)
            );

            $db = new PDO ($this->db->dsn,$this->db->username, $this->db->password, $this->db->options);
            $sql = $db->prepare("
                INSERT INTO
                  phpclass.member_login(fullname, email, password, role_id, member_key)
                  VALUE(:FullName, :Email, :Password, :RoleID, :MemberKey)
            ");
            $sql->bindValue(':FullName', $fullname);
            $sql->bindValue(':Email', $email);
            $sql->bindValue(':Password', md5($pwd . $member_key));
            $sql->bindValue(':RoleID', 2);
            $sql->bindValue(':MemberKey', $member_key);
            $sql->execute();
            return true;
        } catch (PDOException $e) {
            echo "DB ERROR => " . $e->getMessage();
            exit;
            return false;
        }
    }
}
<?php
session_start();
//unset($_SESSION['UID']);

include('../includes/db_conn.php');

$error = '';

if (isset($_POST['txt_email']) && isset($_POST['txt_password']) ) {

    $email = $_POST['txt_email'];
    $submitted_password = $_POST['txt_password'];

    try {
        //-- look up the user data by email address in the member table
        $db = new PDO ($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('
            SELECT
              password,
              role_id,
              member_key
            FROM
              phpclass.member_login
            WHERE
              email = :Email
        ');
        $sql->bindValue(':Email', $email);
        $sql->execute();
        $row = $sql->fetch();

        if ($row != null) {
            //--check passwords if email exists
            $hashed_password = md5($submitted_password . $row['member_key']);

            if ($hashed_password == $row['password'] && $row['role_id'] == 1) {
                $_SESSION['UID'] = $row['member_key'];
                $_SESSION['ROLE'] = $row['role_id'];
                header("Location:admin.php");
                exit;
            } else if ($hashed_password == $row['password'] && $row['role_id'] > 1){
                $_SESSION['UID'] = $row['member_key'];
                $_SESSION['ROLE'] = $row['role_id'];
                header("Location:member.php");
                exit;
            } else {
                $error = "Wrong email or password";
            }
        }else {
            $error = 'Wrong email or password';
        }

        print_r($row);exit;

    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }

//    if( strtolower($email) == 'admin' && $password == 'p@ss' ) {
//        $_SESSION['UID'] = 1;
//        header("Location:admin.php");
//    } else if ( strtolower($email) == 'member' && $password == 'p@ss') {
//        header("Location:member.php");
//    } else {
//        $error = 'Invalid Username or Password';
//    }
}


?>





<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <form method="post">

        <p class ="error"><?= $error; ?></p>
        <table border="1" width="80%">
            <tr height="100">
                <th colspan="2"><h3>Log in:</h3></th>
            </tr>

            <tr height="50">
                <th>Email</th>
                <td><input type="text" name="txt_email" id="txt_email" size="50" value="<?= $email ?>"/></td>
            </tr>

            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="txt_password" id="txt_password" size="50" value="<?= $password ?>"/></td> <!-- TODO: Change input type to "password" -->
            </tr>

            <tr height="50">
                <td colspan="2">
                    <p>
                        <input type="submit" value="login" />
                    </p>
                </td>
            </tr>
        </table>
    </form>`
</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
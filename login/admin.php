<?php
    session_start();

    if( !isset($_SESSION['ROLE']) || $_SESSION['ROLE'] != 1 ) {
        header("Location:index.php");
    }

    $error = '';

    if ( isset($_POST['submit']) ) {
        //exit("Here test!");

        //--validation checks
        if ( isset($_POST['txt_fullname']) && !empty($_POST['txt_fullname']) ) {
            $fullName = $_POST['txt_fullname'];
        } else {
            $error = 'Full name required.';
        }

        if ( isset($_POST['txt_email']) && !empty($_POST['txt_email']) ) {
            $email = $_POST['txt_email'];
        } else {
            $error = 'Email required.';
        }

        if ( isset($_POST['txt_password']) && !empty($_POST['txt_password']) ) {
            $password = $_POST['txt_password'];
        } else {
            $error = 'Password required.';
        }

        if ( isset($_POST['txt_verify_password']) && !empty($_POST['txt_verify_password']) ) {
            $verify_password = $_POST['txt_verify_password'];
        } else {
            $error = 'Please verify your password.';
        }

        if ( isset($_POST['txt_role']) && !empty($_POST['txt_role']) ) {
            $role = $_POST['txt_role'];
        } else {
            $error = 'Role required.';
        }

        if ( $password != $verify_password) {
            $error = 'Passwords must match.';
        }

        include('../includes/db_conn.php'); // include DB connection


        try {
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("SELECT member_id FROM phpclass.member_login WHERE email = :Email");
            $sql->bindValue(':Email', $email);
            $sql->execute();
            $row = $sql->fetch();

            if ($row != null) {
                $error = $email . " already exists.";
            }

        }catch (PDOException $e) {
            echo $e->getMessage();
            exit;
        }


        //--DB record insert
        if(empty($error) ) {
            //$error = "insert DB stuff.";

            //--database stuff
            try{
                $member_key = sprintf(
                    '%04X%04X%04X%04X%04X%04X%04X%04X',
                    mt_rand(0, 65535),
                    mt_rand(0, 65535),
                    mt_rand(0, 65535),
                    mt_rand(16384, 20479),
                    mt_rand(32768, 49151),
                    mt_rand(0, 65535),
                    mt_rand(0, 65535),
                    mt_rand(0, 65535)
                );

                $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
                $sql = $db->prepare("
                INSERT INTO
                  phpclass.member_login(fullname, email, password, role_id, member_key)
                  VALUE(:FullName, :Email, :Password, :RoleID, :MemberKey)
            ");
                $sql->bindValue(':FullName', $fullName);
                $sql->bindValue(':Email', $email);
                $sql->bindValue(':Password', md5($password . $member_key));
                $sql->bindValue(':RoleID', $role);
                $sql->bindValue(':MemberKey', $member_key);
                $sql->execute();
            } catch (PDOException $e) {
                echo "DB ERROR => " . $e->getMessage();
                exit;
            }

            $error = 'New Member Added.';
            unset($fullName, $email, $password, $role);
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h3>Admin Page</h3>

    <form method="post">

        <p class ="error"><?= $error; ?></p>
        <table border="1" width="80%">
            <tr height="100">
                <th colspan="2"><h3>Account Admin</h3></th>
            </tr>

            <tr height="50">
                <th>Full Name</th>
                <td><input type="text" name="txt_fullname" id="txt_fullname" size="50" value="<?= $fullName ?>"/></td>
            </tr>

            <tr height="50">
                <th>Email</th>
                <td><input type="text" name="txt_email" id="txt_email" size="50" value="<?= $email ?>"/></td>
            </tr>

            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="txt_password" id="txt_password" size="50" value="<?= $password ?>"/></td>
            </tr>

            <tr height="50">
                <th>Verify Password</th>
                <td><input type="password" name="txt_verify_password" id="txt_verify_password" size="50" value="<?= $verify_password ?>"/></td>
            </tr>

            <tr height="50">
                <th>Role</th>
                <td>
                    <select name="txt_role" id="txt_role">
                        <?php
                        include('../includes/db_conn.php'); // include DB connection
                        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
                        $sql = $db->prepare("SELECT * FROM phpclass.role");
                        $sql->execute();
                        $result = $sql->fetchAll();
                        foreach ($result as $k => $v) { // https://stackoverflow.com/questions/66894427/populating-a-dropdown-list-from-database-variable-php

                            $role_name = $v['role_value'];
                            $role_id = $v['role_id'];

                            echo "<option value='$role_id'";
                            if ($role == $role_id) {echo "selected";}
                            echo "> $role_name </option>";
                        }
                        ?>
<!--                        <option value="1" --><?php //if($role == 1) {echo "selected";} ?><!-->--><?php //$role_name_1?><!--</option>-->
<!--                        <option value="2" --><?php //if($role == 2) {echo "selected";} ?><!-->Member</option>-->
<!--                        <option value="3" --><?php //if($role == 3) {echo "selected";} ?><!-->Operator</option>-->
                    </select>
                </td>
            </tr>

            <tr height="50">
                <td colspan="2">
                    <p>
                        <input type="submit" name="submit" value="Create User Account" />
                    </p>
                </td>
            </tr>
        </table>
    </form>
</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
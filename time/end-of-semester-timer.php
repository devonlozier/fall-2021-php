<?php

// -- defining some constants involving time for math calculations.
$SEC_PER_MIN = 60;
$SEC_PER_HOUR = 3600;
$SEC_PER_DAY = 3600 * 24;
$SEC_PER_YEAR = 365 * $SEC_PER_DAY;

$NOW = time();
$SEMESTER_END = mktime(0, 0, 0, 12, 13, 2021);

//-- what's the difference in total seconds between the two dates?
$seconds = $SEMESTER_END - $NOW;

// -- calculating the number of years
$years = floor($seconds / $SEC_PER_YEAR);
//echo "$years";exit;
// -- remove the years(in seconds) from our total number of seconds
$seconds = $seconds - ($SEC_PER_YEAR * $years);

// -- calc the number of days
$days = floor($seconds / $SEC_PER_DAY);
// -- remove the days(in seconds) from our total number of seconds.
$seconds = $seconds - ($SEC_PER_DAY * $days);

// -- calc the number of hours
$hours = floor($seconds / $SEC_PER_HOUR);
// -- remove the hours(in seconds) from our total number of seconds.
$seconds = $seconds - ($SEC_PER_HOUR * $hours);

// -- calc the number of minutes
$mins = floor($seconds/$SEC_PER_MIN);
// -- remove the hours(in seconds) from our total number of seconds.
$seconds = $seconds - ($SEC_PER_MIN * $mins);

//echo "$years years | $days days | $hours hours | $mins mins | $seconds seconds";exit;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h1>End of Semester Countdown Timer</h1>

    <p>End of Semester(in seconds): <?= $SEMESTER_END ?></p>
    <p>End of Semester <?= date("Y-m-d H:i:s", $SEMESTER_END) ?></p> <!-- Year month day ... Hours:minutes:seconds-->
    <p>Current Datetime: <?= date("Y-m-d H:i:s") ?></p>
    <p>Years: <?= $years ?> | Days: <?= $days ?> | Hours: <?= $hours ?> | Minutes: <?= $mins ?> | Seconds: <?= $seconds ?></p>

    <p>Years: <?= $years ?></p>
    <p>Days: <?= $days ?></p>
    <p>Hours: <?= $hours ?></p>
    <p>Minutes: <?= $mins ?></p>
    <p>Seconds: <?= $seconds ?></p>

</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
<?php
    include('../includes/db_conn.php');

    $error = '';

    if (isset($_POST['movie_name']) && !empty($_POST['movie_name'])) {

        if (isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])) {
            $id = $_POST['movie_id'];
            $title = $_POST['movie_name'];
            $rating = $_POST['movie_rating'];

            //--database stuff
            try{
                $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
                $sql = $db->prepare("
                    UPDATE
                      phpclass.movielist
                    SET
                      movie_title = :Title,
                      movie_rating = :Rating
                    WHERE
                      movie_id = :Id
            ");
                $sql->bindValue(':Title', $title);
                $sql->bindValue(':Rating', $rating);
                $sql->bindValue(':Id', $id);
                $sql->execute();

                header("Location:list.php?update=1");
                exit;

            } catch (PDOException $e) {
                echo "DB ERROR => " . $e->getMessage();
                exit;
            }
        }
    }

    if (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT)) {

        $id = $_GET['id'];
//        print_r($_GET['id']);exit;

        try {
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare('
                  SELECT 
                    movie_title, movie_rating 
                  FROM 
                    phpclass.movielist 
                  WHERE 
                    movie_id = :Id
            ');
            $sql->bindValue(":Id", $id);
            $sql->execute();
            $row = $sql->fetch();

//            echo"<pre>";
//            print_r($row);
//            echo"</pre>";
//            exit;

            $title = $row['movie_title'];
            $rating = $row['movie_rating'];

//            echo $title . " => " . $rating;

        } catch (PDOException $e) {
            echo "DB ERROR => " . $e->getMessage();
            exit;
        }

    } else {
        header("Location:list.php");
        exit;
    }

?>





<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <script type="text/javascript">

        function DeleteMovie(title, id) {
//            alert(title + " => " + id)

            if( confirm("Delete the database record for " + title + "?") ){

//                alert(title + " => " + id)
                document.location.href="delete.php?id="+id+"&title="+title;

            }
        }
    </script>
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <form method="post">
        <input type="hidden" name="movie_id" id="movie_id" value="<?= $id  ?>" /> <!-- $_GET['id'] -->
        <p class ="error"><?= $error; ?></p>
        <table border="1" width="80%">
        <tr height="100">
            <th colspan="2"><h3>Update Movie</h3></th>
        </tr>

        <tr height="50">
            <th>Movie Name</th>
            <td><input type="text" name="movie_name" id="movie_name" size="50" value="<?= $title ?>"/></td>
        </tr>

        <tr height="50">
            <th>Rating</th>
            <td><input type="text" name="movie_rating" id="movie_rating" size="10" value="<?= $rating ?>"/></td>
        </tr>

        <tr height="50">
            <td colspan="2">
                <p>
                    <input type="submit" value="update movie" />
                    <input type="button" value="delete movie" onClick="DeleteMovie('<?= $title ?>', '<?= $id ?>')" />
                </p>
            </td>
        </tr>
        </table>
    </form>
</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
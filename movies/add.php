<?php
    $error = '';
    $title = '';
    $rating = '';
/*
    if(
        isset($_POST['movie_name']) && !empty($_POST['movie_name'])
        && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])
    ){
        $title = $_POST['movie_name'];
        $rating = $_POST['movie_rating'];

        echo $title . " => " . $rating; exit;
    } else if(isset($_POST) && !empty($_POST)){
        $error = "Please ensure that both a title and rating are provided before submitting the new movie.";

    }
*/
    if(isset($_POST['movie_name']) && !empty($_POST['movie_name'])) {
        $title = $_POST['movie_name'];
    }
    if(isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])) {
        $rating = $_POST['movie_rating'];
    }

    if(!empty($title) && !empty($rating)) {
        //echo $title . " => " . $rating; exit;

        //--database stuff
        include('../includes/db_conn.php');
        try{
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("
                INSERT INTO
                  phpclass.movielist(movie_title, movie_rating)
                  VALUE(:Title, :Rating)
            ");
            $sql->bindValue(':Title', $title);
            $sql->bindValue(':Rating', $rating);
            $sql->execute();

            header("Location:list.php?success=1");
            exit;

        } catch (PDOException $e) {
            echo "DB ERROR => " . $e->getMessage();
            exit;
        }

    } else if (isset($_POST) && !empty($_POST)) {
        $error = "Please ensure that both a title and rating are provided before submitting the new movie.";
}
?>





<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <form method="post">
        <p class ="error"><?= $error; ?></p>
        <table border="1" width="80%">
        <tr height="100">
            <th colspan="2"><h3>Add new Movie</h3></th>
        </tr>

        <tr height="50">
            <th>Movie Name</th>
            <td><input type="text" name="movie_name" id="movie_name" size="50" value="<?= $title ?>"/></td>
        </tr>

        <tr height="50">
            <th>Rating</th>
            <td><input type="text" name="movie_rating" id="movie_rating" size="10" value="<?= $rating ?>"/></td>
        </tr>

        <tr height="50">
            <td colspan="2"><input type="submit" value="add_movie" /></td>
        </tr>
        </table>
    </form>
</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
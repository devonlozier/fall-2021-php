<?php
    include('../includes/db_conn.php');

    try{
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options); // call pizza hut
        $sql = $db->prepare("SELECT * FROM phpclass.movielist"); // place your order
        $sql->execute(); // baking the pizza
        $rows = $sql->fetchAll(); // deliver the pizza
        //print_r($rows);exit; // eating your pizza

    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Movie List</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h3>Movie List</h3>
    <?php if(isset($_GET['success']) && !empty($_GET['success'])){ ?>
        <p class="success">Movie Added</p>
    <?php } ?>

    <?php if(isset($_GET['update']) && !empty($_GET['update'])){ ?>
        <p class="success">Movie Updated</p>
    <?php } ?>

    <?php if(isset($_GET['delete']) && !empty($_GET['delete'])){ ?>
        <p class="success"><?= $_GET['title'] ?> Deleted</p>
    <?php } ?>

    <table border="1" width="80%">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Rating</th>
        </tr>
        <?php foreach($rows as $row) { ?>
            <tr>
                <td><?= $row['movie_id']?></td>
                <td><a href="update.php?id=<?= $row['movie_id'] ?>"><?= $row['movie_title']?></a></td> <!-- id=row movie ID using php. For the GET -->
                <td><?= $row['movie_rating']?></td>
            </tr>
        <?php } ?>
    </table>
    <p>
        <a href="add.php">add new movie</a>
    </p>

</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
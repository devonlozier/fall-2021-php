<?php
if (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT)) {

    $id = $_GET['id'];

    $title = $_GET['title'];

    try {
        include('../includes/db_conn.php');

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('
                  DELETE 
                  FROM 
                    phpclass.movielist 
                  WHERE 
                    movie_id = :Id
            ');
        $sql->bindValue(":Id", $id);
        $sql->execute();

        header("Location:list.php?delete=1&title=".$title);
        exit;

    } catch (PDOException $e) {
        echo "DB ERROR => " . $e->getMessage();
        exit;
    }

} else {
    header("Location:list.php");
    exit;
}

?>
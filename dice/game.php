<?php
//--variables for user dice
$dice_1 = mt_rand(1,6);
$dice_2 = mt_rand(1,6);
$user_score = $dice_1 + $dice_2;

//--variables for computer dice
$cpu_dice_1 = mt_rand(1,6);
$cpu_dice_2 = mt_rand(1,6);
$cpu_dice_3 = mt_rand(1,6);
$cpu_score = $cpu_dice_1 + $cpu_dice_2 + $cpu_dice_3;

//--result
$result = "Could not be determined.";

if ($user_score > $cpu_score)
    $result = "Victory!";
else if ($user_score == $cpu_score)
    $result = "Draw.";
else
    $result = "Loss. Honestly, the computer cheats anyways. Who gave that guy an extra die?";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dice Game</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>



<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h2>Your score: <?= $user_score ?> </h2>
    <img src="img/dice_<?=$dice_1?>.png" alt="User dice 1">
    <img src="img/dice_<?=$dice_2?>.png" alt="User dice 2">

    <h2>Computer Score: <?= $cpu_score?> </h2>
    <img src="img/dice_<?=$cpu_dice_1?>.png" alt="Computer dice 1">
    <img src="img/dice_<?=$cpu_dice_2?>.png" alt="Computer dice 2">
    <img src="img/dice_<?=$cpu_dice_3?>.png" alt="Computer dice 3">

    <h2>Result: <?=$result?></h2>

</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>

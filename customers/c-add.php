<?php

$resultChecking ='';
$error = array();
$firstName = '';
$lastName = '';
$phoneNumber = '';
$email = '';
$address = '';
$city = '';
$zipCode = '';
$state = '';
$password = '';
$passwordVerify = '';


if(isset($_POST['first_name']) && !empty($_POST['first_name'])) {
    $firstName = $_POST['first_name'];
}
if(isset($_POST['last_name']) && !empty($_POST['last_name'])) {
    $lastName = $_POST['last_name'];
}
if(isset($_POST['phone_number']) && !empty($_POST['phone_number'])) {
    $phoneNumber = $_POST['phone_number'];
}
if(isset($_POST['email']) && !empty($_POST['email'])) {

    $emailCheck = $_POST['email'];


    if (strpos($emailCheck, '@') !== false) {
        $emailExplode = explode("@", $emailCheck);

        if (strpos($emailExplode[1], '.com') !== false)
            $email = $_POST['email'];
    }

}
if(isset($_POST['address']) && !empty($_POST['address'])) {
    $address = $_POST['address'];
}
if(isset($_POST['city']) && !empty($_POST['city'])) {
    $city = $_POST['city'];
}
if(isset($_POST['zip']) && !empty($_POST['zip'])) {
    $zipCode = $_POST['zip'];
}
if(isset($_POST['state']) && !empty($_POST['state'])) {
    $state = $_POST['state'];
}
if(isset($_POST['password']) && !empty($_POST['password']) && $_POST['password'] == $_POST['password_verify'] ) {
    $password = $_POST['password'];
//    echo('Password verified');
//    exit;
}
//if(isset($_POST['password_verify']) && !empty($_POST['password_verify'])) {
//    $passwordVerify = $_POST['password_verify'];
//}

if (isset($_POST) && !empty($_POST)) {
    if (!empty($password) /*&& $password == $passwordVerify && !empty($passwordVerify)*/) {
        if(!empty($firstName))
            if(!empty($lastName))
                if (!empty($phoneNumber))
                    if (!empty($email))
                        if (!empty($address))
                            if (!empty($city))
                                if (!empty($zipCode))
                                    if (!empty($state)) {

                                        //--database stuff
                                        include('../includes/db_conn.php');
                                        try{
                                            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
                                            $sql = $db->prepare("
                                                    INSERT INTO
                                                      phpclass.CustomerList(Address, LastName, FirstName, City, State, Zip, Phone, Email, Password)
                                                      VALUE(:Address, :LastName, :FirstName, :City, :State, :Zip, :Phone, :Email, :Password)
                                                ");
                                            $sql->bindValue(':Address', $address);
                                            $sql->bindValue(':LastName', $lastName);
                                            $sql->bindValue(':FirstName', $firstName);
                                            $sql->bindValue(':City', $city);
                                            $sql->bindValue(':State', $state);
                                            $sql->bindValue(':Zip', $zipCode);
                                            $sql->bindValue(':Phone', $phoneNumber);
                                            $sql->bindValue(':Email', $email);
                                            $sql->bindValue(':Password', md5('salting' . $password . '1791'));

                                            $sql->execute();

                                            header("Location:c-list.php?success=1");
                                            exit;

                                        } catch (PDOException $e) {
                                            echo "DB ERROR => " . $e->getMessage();
                                            exit;
                                        }
                                    }
                                    else
                                        array_push($error, "State isn't working.");
                                else
                                    array_push($error, "Zip isn't working.");
                            else
                                array_push($error, "City isn't working.");
                        else
                            array_push($error,"Address isn't working.");
                    else
                        array_push($error, "Email isn't working.");
                else
                    array_push($error, "Phone number isn't working.");
            else
                array_push($error, "Last name isn't working.");
        else
            array_push($error, "First Name isn't working.");
    }
    else
        array_push($error, "Please verify your password.");
}

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <form method="post" style="width:800px;">
        <p class ="error"><?= implode(",", $error); ?> <?= $resultChecking; ?> </p>

        <h3>Add new Customer</h3>

        <fieldset>
            <legend>Customer</legend>
            <ul style="width:600px; text-align:left;">
                <li><label for="first_name">First Name:</label>
                    <input type="text" name="first_name" id="first_name" size="30" placeholder="first name" value="<?= $firstName ?>" required />*</li>

                <li><label for="last_name">Last Name:</label>
                    <input type="text" name="last_name" id="last_name" placeholder="last name" size="30" value="<?= $lastName ?>" required/>*</li>

                <li><label for="phone_number">Phone Number:</label>
                    <input type="tel" name="phone_number" id="phone_number" size="15" value="<?= $phoneNumber ?>" placeholder="(###)###-#####" required/>*</li>

                <li><label for="email">Email:</label>
                    <input type="email" name="email" id="email" placeholder="youraddress@gmail.com" size="60" value="<?= $email ?>" required/>*</li>
            </ul>
        </fieldset>

        <fieldset>
            <legend>Address</legend>
            <ul style="width:600px; text-align:left">
                <li><label for="address">Address:</label>
                    <input type="text" name="address" id="address" size="40" placeholder="address" value="<?= $address ?>" required />*</li>

                <li><label for="city">City:</label>
                    <input type="text" name="city" id="city" placeholder="appleton" size="30" value="<?= $city ?>" required/>*</li>

                <li><label for="zip">Zip Code:</label>
                    <input type="text" name="zip" id="zip" size="10" placeholder="54911" value="<?= $zipCode ?>" required/>*</li>

                <li><label for="state">State:</label>
                    <input type="text" name="state" id="state" placeholder="WI" size="2" value="<?= $state ?>" required/>*</li>
            </ul>
        </fieldset>

        <fieldset>
            <legend>Security</legend>
            <ul style="width:600px; text-align: left;">
                <li><label for="password">Password:</label>
                    <input type="password" name="password" id="password" size="60" placeholder="******" value="<?= $password ?>" required/>*</li>

                <li><label for="password_verify">Verify Password:</label>
                    <input type="password" name="password_verify" id="password_verify" placeholder="verify password" size="60" value="<?= $passwordVerify ?>" required/>*</li>
            </ul>
        </fieldset>

        <input type="submit" value="Create Account" />
    </form>
</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
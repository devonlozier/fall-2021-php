<?php
if (isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT)) {

    $id = $_GET['id'];

    try {
        include('../includes/db_conn.php');

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('
                  DELETE 
                  FROM 
                    phpclass.CustomerList
                  WHERE 
                    CustomerID = :Id
            ');
        $sql->bindValue(":Id", $id);
        $sql->execute();

        header("Location:c-list.php?delete=1");
        exit;
        
    } catch (PDOException $e) {
        echo "DB ERROR => " . $e->getMessage();
        exit;
    }

} else {
    header("Location:c-list.php");
    exit;
}

?>
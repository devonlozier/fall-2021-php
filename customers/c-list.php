<?php
include('../includes/db_conn.php');

try{
    $db = new PDO($db_dsn, $db_username, $db_password, $db_options); // call pizza hut
    $sql = $db->prepare("SELECT * FROM phpclass.CustomerList"); // place your order
    $sql->execute(); // baking the pizza
    $rows = $sql->fetchAll(); // deliver the pizza
    //print_r($rows);exit; // eating your pizza

} catch (PDOException $e) {
    echo $e->getMessage();
    exit;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Movie List</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h3>Customer List</h3>
    <?php if(isset($_GET['success']) && !empty($_GET['success'])){ ?>
        <p class="success">Customer Added</p>
    <?php } ?>
    <?php if(isset($_GET['update']) && !empty($_GET['update'])){ ?>
        <p class="success">Customer Updated</p>
    <?php } ?>
    <?php if(isset($_GET['delete']) && !empty($_GET['delete'])){ ?>
        <p class="success">Customer Deleted</p>
    <?php } ?>
    <table border="1" width="80%">
        <tr>
            <th>CustomerID</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Password</th>
        </tr>
        <?php foreach($rows as $row) { ?>
            <tr>
                <td><?= $row['CustomerID']?></td>
                <td><a href="c-update.php?id=<?= $row['CustomerID'] ?>"><?= $row['FirstName']?></a></td> <!-- id=$row['CustomerID'] using php. For the GET -->
                <td><?= $row['LastName']?></td>
                <td><?= $row['Address']?></td>
                <td><?= $row['City']?></td>
                <td><?= $row['State']?></td>
                <td><?= $row['Zip']?></td>
                <td><?= $row['Phone']?></td>
                <td><?= $row['Email']?></td>
                <td><?= $row['Password']?></td>
            </tr>
        <?php } ?>
    </table>
    <p>
        <a href="c-add.php">add new customer</a>
    </p>

</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h1>Loops Demo</h1>

    <?php
    echo "Printing out vars <br/><br/>";
    $number = 100;
    //print $number;
    echo "<h3>".$number."</h3>"; //same as the 3 lines below.

    echo "<h3>";
    echo $number;
    echo "</h3>";

    echo "<h3>$number</h3>";
    echo '<h3>$number</h3>';
    echo "<h3>{$number}</h3>";

    $result = "<h1>";
    $result .= $number; // $result = $result . $number; // . is concatenation
    $result .= "</h1"; // $result = $result . "</h1>";
    echo $result;

    echo "basic math with PHP <br/><br/>";
    $number1 = 100;
    $number2 = "ABC 50";
    $result = ($number1 . $number2); // maybe + should be used? Dunno this is just weird in general.
    echo "<h3>$result</h3>";

    echo "while loop <br/><br/>";

    $i = 1;
    while($i < 7) {
        echo "<h$i>Howdy World!</h$i>";

        $i++; // $i = $i + 1; // $i += 1;
    }

    $i = 6;
    while($i > 0) {
        echo "<h$i>Howdy World!</h$i>";
        $i--;
    }

    echo "do-while loop <br/><br/>";
    $i = 6;
    do {
        echo "<h$i>Howdy World!</h$i>";
        $i--;
    }while($i > 0);

    echo "for loop <br/><br/>";

    for($i = 1; $i < 6; $i++){
        echo "<h$i>Howdy World!</h$i>";
    }

    echo "String Functions <br/><br/>";
    $full_name = "Bobby Smith";
    //$full_name = 123; // would 1.2.3 work for first . middle . last ?
    //$full_name = false;

    // b o b b y   s m i t h
    // 0 1 2 3 4 5 6 7 8 9 10

    $position = strpos($full_name, ' ');
    echo "<br/>The space is in position $position <br/>";

    echo "<br/>$full_name<br/>";
    echo "<br/>".strtoupper($full_name)."<br/>";
    echo "<br/>".strtolower($full_name)."<br/>";

    $name_parts = explode(' ', $full_name);
    echo "<br/>First Name: " . $name_parts[0];
    echo "<br/>Last Name: " . $name_parts[1];

    ?>

</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
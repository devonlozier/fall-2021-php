<?php
    session_start(); // starts session checking the backend server.

    $answer = 'Seek you knowledge? Ask away.';
    $question = '';

    if(isset($_POST['txtQuestion'])){
        $question    = $_POST['txtQuestion'];
        //var_dump($question);exit;
    }

    $previous_question = '';
    if(isset($_SESSION['previous_question'])){
        $previous_question = $_SESSION['previous_question'];

    }


    $responses = [
        'Ask Again Later', // -- 0 index
        'Yes', // -- 1 index
        'No', // 2
        'It appears to be so', // 3
        'Reply is Hazy, please try again', // 4
        'Yes, definitely', // 5
        'What is it you really want to know', // 6
        'Outlook is Good', // 7
        'My sources say no', // 8
        'Signs point to yes', // 9
        "Don't count on it", // 10 // backslash (\) is an escape character.
        'Cannot predict now', //11
        'As I see it, Yes', // 12
        'Better not tell you now', // 13
        'Concentrate and ask again' // 14
    ];

    if(empty($question)){
        $answer = 'For answers, you must speak. Ask away.';
    } else if(substr($question, -1) != '?') // -1 checks the FINAL character in a string.
    {
        $answer = "Are you sure your question has a question mark at the end?";
    }  else if ($previous_question == $question) {
        $answer = "I will not answer this question again so soon.";
    }
    else {
        $answer = $responses[mt_rand(0, 14)/*rand(0,14)*/];
        $_SESSION['previous_question'] = $question;
    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Devon Lozier's Assignment Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php') ?></header>

<nav><?php include('../includes/nav.php') ?></nav>

<main>
    <!-- insert assignment content here -->
    <h1>Magic 8 Ball</h1>

    <p>
        <marquee><?= $answer ?></marquee>
    </p>

    <form method="post" action="8ball.php" title="8 Ball"> <!-- Other method="get" -->
        <p>Ask me a question:</p>
        <p><input type="text" name="txtQuestion" id="txtQuestion"></p>
        <p><input type="submit" value="Ask the 8 Ball"</p>
    </form>

</main>

<footer><?php include('../includes/footer.php') ?></footer>
</body>
</html>
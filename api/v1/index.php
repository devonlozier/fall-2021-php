<?php
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/** CRUD: Create Read Update Delete
 * get    = select
 * post   = insert
 * put    = update
 * delete = delete
 */

$app = new \Slim\Slim();

$app->get('/getHello', 'getHello');
$app->get('/showMember/:memberName','showMember');
$app->post('/addMember/:memberName','addMember');
$app->post('/addJson','addJson');
$app->delete('/delUser/:userId','delUser');

//homework
$app->get('/get-races', 'getRaces');
$app->get('/get-runners/:race_id','getRunners');
$app->post('/add-runner/','addRunner');
$app->delete('/delete-runner/','deleteRunner');
$app->put('/update-runner','updateRunner'); //for update: patch(), put()

$app->run();

//Class demo
function getHello(){
    echo "Hello World";
}

function showMember($memberName){
    echo "Show $memberName";
}

function addMember($memberName){
    echo "Creating $memberName";
}

function addJson(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);
    echo $post_json['fname'];
}

function delUser($userId){
    echo "Deleting user ID $userId";
}

//Homework
function getRaces(){
    include('../../includes/db_conn.php');

    try{
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('SELECT * FROM phpclass.race_events');
        $sql->execute();
        $rows = $sql->fetchAll();
        $results['runners'] = $rows;
        echo json_encode($results);
    } catch(PDOException $e) {
        $errors['error'] = $e->getMessage();
        echo json_encode($errors);
    }
}
function getRunners($race_id){

    include('../../includes/db_conn.php');

    try{
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('SELECT
                                 member_login.email, 
                                 member_login.fullname, 
                                 member_race.race_id
                             FROM
                                 member_login
                             INNER JOIN
                                 member_race
                             ON 
                                 member_race.member_id = member_login.member_id
                             WHERE
                                 member_race.race_id = :Raceid

        ');
        $sql->bindValue(':Raceid', $race_id);
        $sql->execute();
        $rows = $sql->fetchAll();
        $results['runners'] = $rows;
        echo json_encode($results);
    } catch(PDOException $e) {
        $errors['error'] = $e->getMessage();
        echo json_encode($errors);
    }
}
function addRunner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);

    $member_id = $post_json['member_id'];
    $race_id = $post_json['race_id'];
    $member_key = $post_json['member_key'];

//    echo "Adding runner: $post_json";
//    echo $member_id . " => " . $race_id . " => " . $member_key;

    include('../../includes/db_conn.php');

    try{
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('SELECT
                                member_race.race_id
                             FROM
                                member_login
                             INNER JOIN
                                member_race
                             ON 
                                member_login.member_id = member_race.member_id
                             WHERE
                                member_login.member_key = :Apikey AND
                                member_race.race_id = :Raceid AND
                                member_race.role_id = 3 

        ');
        $sql->bindValue(':Apikey', $member_key);
        $sql->bindValue(':Raceid', $race_id);
        $sql->execute();
        $rows = $sql->fetchAll();

        if ($rows == null) {
            echo "Bad API Key";
        } else {
//            echo "Insert our Runner!";exit;
            $sql = $db->prepare('
                INSERT INTO
                  phpclass.member_race
                    (member_id, race_id, role_id)
                VALUES
                    (:Memberid, :Raceid, 2)
            ');
            $sql->bindValue(':Memberid', $member_id);
            $sql->bindValue(':Raceid', $race_id);
            $sql->execute();
        }
    } catch(PDOException $e) {
        $errors['error'] = $e->getMessage();
        echo json_encode($errors);
    }
}
function deleteRunner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);
//    echo "Deleted runner: $post_json";

    $member_id = $post_json['member_id'];
    $race_id = $post_json['race_id'];
    $member_key = $post_json['member_key'];

    include('../../includes/db_conn.php');

    try{
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare('SELECT
                                member_race.race_id
                             FROM
                                member_login
                             INNER JOIN
                                member_race
                             ON 
                                member_login.member_id = member_race.member_id
                             WHERE
                                member_login.member_key = :Apikey AND
                                member_race.race_id = :Raceid AND
                                member_race.role_id = 3 

        ');
        $sql->bindValue(':Apikey', $member_key);
        $sql->bindValue(':Raceid', $race_id);
        $sql->execute();
        $rows = $sql->fetchAll();

        if ($rows == null) {
            echo "Bad API Key";
        } else {
//            echo "Delete our Runner!";exit;
            $sql = $db->prepare('
                DELETE FROM
                  phpclass.member_race
                WHERE
                   phpclass.member_race.member_id = :Memberid AND
                   member_race.race_id = :Raceid AND
                   member_race.role_id = 2
                   
            ');
            $sql->bindValue(':Memberid', $member_id);
            $sql->bindValue(':Raceid', $race_id);
            $sql->execute();
        }
    } catch(PDOException $e) {
        $errors['error'] = $e->getMessage();
        echo json_encode($errors);
    }
}
function updateRunner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);
    echo "Updated runner: $post_json";
}
<?php
class Car {

    // -- class properties
    //private, protected, public

    /**
     * The car color
     *
     * @var string
     */
    public $color;

    /**
     * the car make (i.e. Ford, Chevy, Jeep, etc.)
     *
     * @var string
     */
    public $make;

    /**
     * the car model (i.e. F150, Focus, Escape, Wrangler, etc.)
     *
     * @var string
     */
    public $model;

    /**
     * The model year of the car
     *
     * @var int
     */
    public $year;

    /**
     * The car's current action (forward, reverse, stop)
     *
     * @var string
     */
    public $status;

    /**
     * Car constructor.
     */
    function __construct()
    {
        $this->status = 'stopped';
        //$this->setStatus('stopped'); // could set this through a function.
    }


    /**
     * move the car forward
     */
    function forward()
    {
        echo "The car is moving forward <br/> <br/>";
        $this->status = 'forward';
    }

    /**
     * move the car in reverse
     */
    function reverse()
    {
        echo "The car is moving backward <br/> <br/>";
        $this->status = 'reverse';
    }

    /**
     * stop the car.
     */
    function stop()
    {
        echo "The car is stopped <br/> <br/>";
        $this->status = 'stopped';
    }

    /**
     * set the color of the car
     *
     * @param string $color
     */
    function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * return the color.
     *
     * @return string $color
     */
    function getColor()
    {
        return $this->color;
    }

    function print_info()
    {

    }
} // -- end of car class

$my_first_car = new Car();
$my_first_car->color = 'green';
$my_first_car->make = 'Dodge';
$my_first_car->model = 'Shadow';
$my_first_car->year = 1994;

echo "My first car was a $my_first_car->color $my_first_car->year a $my_first_car->make $my_first_car->model. <br/> <br/>>";
$my_first_car->forward();
$my_first_car->reverse();
$my_first_car->stop();

$my_current_car = new Car();
$my_first_car->color = 'grey';
$my_first_car->make = 'Ram';
$my_first_car->model = '1500';
$my_first_car->year = 2018;